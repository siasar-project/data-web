import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { calculateScore } from 'lib/Utils';

import ScoreTree from 'components/common/ScoreTree';

class SystemScoreTree extends Component {
  render() {
    const { t } = this.props;
    return (
      <ScoreTree>
        <path d="M255,55 L55,180 Z" className="score-tree-line" />
        <path d="M255,55 L180,180 Z" className="score-tree-line" />
        <path d="M255,55 L330.5,180 Z" className="score-tree-line" />
        <path d="M255,55 L455,180 Z" className="score-tree-line" />
        <g id="wsi" transform="translate(255, 55)">
          <circle r="55" className={`score-tree-circle background-${calculateScore(this.props.wsi)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsi)}`}>
            {t('wsi')}
          </text>
        </g>
        <g id="wsiAut" transform="translate(55, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.wsiAut)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsiAut)}`}>
            {t('aut')}
          </text>
        </g>
        <g id="wsiInf" transform="translate(180, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.wsiInf)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsiInf)}`}>
            {t('inf')}
          </text>
        </g>
        <g id="wsiPro" transform="translate(330, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.wsiPro)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsiPro)}`}>
            {t('pro')}
          </text>
        </g>
        <g id="wsiTre" transform="translate(455, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.wsiTre)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsiTre)}`}>
            {t('tre')}
          </text>
        </g>
      </ScoreTree>
    );
  }
}

SystemScoreTree.propTypes = {
  wsi: PropTypes.number,
  wsiAut: PropTypes.number,
  wsiInf: PropTypes.number,
  wsiPro: PropTypes.number,
  wsiTre: PropTypes.number,
  t: PropTypes.func.isRequired,
};

SystemScoreTree.defaultProps = {
  wsi: null,
  wsiAut: null,
  wsiInf: null,
  wsiPro: null,
  wsiTre: null,
};

export default translate()(SystemScoreTree);
