import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { calculateScore } from 'lib/Utils';

import ScoreTree from 'components/common/ScoreTree';

class ServiceProviderScoreTree extends Component {
  render() {
    const { t } = this.props;
    return (
      <ScoreTree>
        <path d="M255,55 L55,180 Z" className="score-tree-line" />
        <path d="M255,55 L180,180 Z" className="score-tree-line" />
        <path d="M255,55 L330.5,180 Z" className="score-tree-line" />
        <path d="M255,55 L455,180 Z" className="score-tree-line" />
        <g id="sep" transform="translate(255, 55)">
          <circle r="55" className={`score-tree-circle background-${calculateScore(this.props.sep)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.sep)}`}>
            {t('sep')}
          </text>
        </g>
        <g id="sepOrg" transform="translate(55, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.sepOrg)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.sepOrg)}`}>
            {t('org')}
          </text>
        </g>
        <g id="sepOpm" transform="translate(180, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.sepOpm)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.sepOpm)}`}>
            {t('opm')}
          </text>
        </g>
        <g id="sepEco" transform="translate(330, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.sepEco)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.sepEco)}`}>
            {t('eco')}
          </text>
        </g>
        <g id="sepEnv" transform="translate(455, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.sepEnv)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.sepEnv)}`}>
            {t('env')}
          </text>
        </g>
      </ScoreTree>
    );
  }
}

ServiceProviderScoreTree.propTypes = {
  sep: PropTypes.number,
  sepOrg: PropTypes.number,
  sepOpm: PropTypes.number,
  sepEco: PropTypes.number,
  sepEnv: PropTypes.number,
  t: PropTypes.func.isRequired,
};

ServiceProviderScoreTree.defaultProps = {
  sep: null,
  sepOrg: null,
  sepOpm: null,
  sepEco: null,
  sepEnv: null,
};

export default translate()(ServiceProviderScoreTree);
