import React, { Component } from 'react';

import { entityColors } from 'lib/Utils';

import Entities from 'components/common/Entities';

class ServiceProviders extends Component {
  render() {
    return (
      <Entities
        entity="serviceProvider"
        endpoint="service-providers"
        indicator="sep"
        stats
        backgroundColor={entityColors[2]}
      />
    );
  }
}

export default ServiceProviders;
