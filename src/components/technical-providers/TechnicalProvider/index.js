import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { translate } from 'react-i18next';

import Api from 'lib/Api';
import { entityColors } from 'lib/Utils';

import PageHeader from 'components/common/PageHeader';
import ValueBox from 'components/common/ValueBox';
import Picture from 'components/common/Picture';
import PointMap from 'components/common/maps/PointMap';
import JoinList from 'components/common/JoinList';
import InfoBox from 'components/common/InfoBox';
import TechnicalProviderScoreTree from 'components/technical-providers/TechnicalProviderScoreTree';
import IndicatorsList from 'components/common/IndicatorsList';

class TechnicalProvider extends Component {
  constructor(props) {
    super(props);

    const { match: { params } } = props;

    this.state = {
      id: parseInt(params.id, 10),
      technicalProvider: {
        location: [],
        communities: [],
        budget: [],
      },
    };
  }

  componentDidMount() {
    Api.get(`technical-providers/${this.state.id}`)
      .then(technicalProvider => this.setState({ technicalProvider }));
  }

  render() {
    const { t } = this.props;
    const budget = `${this.state.technicalProvider.budget.value} ${this.state.technicalProvider.budget.unit}`;
    return (
      <React.Fragment>
        <PageHeader title={t('technicalProvider')} />
        <Row>
          <Col>
            <InfoBox
              entity={this.state.technicalProvider}
              entityName="technicalProvider"
            />
          </Col>
        </Row>
        <Row>
          <Col sm="6" md="3">
            <ValueBox
              title={t('households')}
              value={this.state.technicalProvider.servedHouseholds}
              style={{ backgroundColor: entityColors[3] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('providerType')}
              value={t(this.state.technicalProvider.providerType)}
              style={{ backgroundColor: entityColors[3] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('techniciansCount')}
              value={this.state.technicalProvider.techniciansCount}
              style={{ backgroundColor: entityColors[3] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('annualBudget')}
              value={budget}
              style={{ backgroundColor: entityColors[3] }}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="3">
            <TechnicalProviderScoreTree
              tap={this.state.technicalProvider.tap}
              tapIs={this.state.technicalProvider.tapIs}
              tapIca={this.state.technicalProvider.tapIca}
              tapCco={this.state.technicalProvider.tapCco}
              tapAin={this.state.technicalProvider.tapAin}
            />
          </Col>
          <Col sm="12" md="6" xl="3">
            <Picture imgUrl={this.state.technicalProvider.pictureUrl} />
          </Col>
          <Col sm="12" md="12" xl="6" className="d-flex">
            <PointMap
              latitude={this.state.technicalProvider.latitude}
              longitude={this.state.technicalProvider.longitude}
              score={this.state.technicalProvider.score}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="6">
            <JoinList
              title={t('community_plural')}
              items={this.state.technicalProvider.communities}
              path="communities"
            />
          </Col>
        </Row>
        <Row>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('financialState')}
              items={[
                {
                  title: `${t('hasAnnualBudget')}`,
                  value: this.state.technicalProvider.annualBudget,
                },
              ]}
            />
          </Col>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('logisticResources')}
              items={[
                {
                  title: `${t('transportEquipment')}`,
                  value: this.state.technicalProvider.transportEquipment,
                },
                {
                  title: `${t('waterQualityEquipment')}`,
                  value: this.state.technicalProvider.equipmentMeasuringWaterQuality,
                },
                {
                  title: `${t('computerEquipment')}`,
                  value: this.state.technicalProvider.computerEquipment,
                },
                {
                  title: `${t('hasTravelFunds')}`,
                  value: this.state.technicalProvider.fundsTravelExpenses,
                },
                {
                  title: `${t('hasFuelFunds')}`,
                  value: this.state.technicalProvider.fundsFuel,
                },
                {
                  title: `${t('hasInternetServices')}`,
                  value: this.state.technicalProvider.internetServices,
                },
              ]}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

TechnicalProvider.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
  t: PropTypes.func.isRequired,
};

export default translate()(TechnicalProvider);
