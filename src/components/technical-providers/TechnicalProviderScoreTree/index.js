import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { calculateScore } from 'lib/Utils';

import ScoreTree from 'components/common/ScoreTree';

class TechnicalProviderScoreTree extends Component {
  render() {
    const { t } = this.props;
    return (
      <ScoreTree>
        <path d="M255,55 L55,180 Z" className="score-tree-line" />
        <path d="M255,55 L180,180 Z" className="score-tree-line" />
        <path d="M255,55 L330.5,180 Z" className="score-tree-line" />
        <path d="M255,55 L455,180 Z" className="score-tree-line" />
        <g id="tap" transform="translate(255, 55)">
          <circle r="55" className={`score-tree-circle background-${calculateScore(this.props.tap)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.tap)}`}>
            {t('tap')}
          </text>
        </g>
        <g id="tapIs" transform="translate(55, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.tapIs)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.tapIs)}`}>
            {t('is')}
          </text>
        </g>
        <g id="tapIca" transform="translate(180, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.tapIca)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.tapIca)}`}>
            {t('ica')}
          </text>
        </g>
        <g id="tapCco" transform="translate(330, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.tapCco)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.tapCco)}`}>
            {t('cco')}
          </text>
        </g>
        <g id="tapAin" transform="translate(455, 180)">
          <circle r="45" className={`score-tree-circle background-${calculateScore(this.props.tapAin)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.tapAin)}`}>
            {t('ain')}
          </text>
        </g>
      </ScoreTree>
    );
  }
}

TechnicalProviderScoreTree.propTypes = {
  tap: PropTypes.number,
  tapIs: PropTypes.number,
  tapIca: PropTypes.number,
  tapCco: PropTypes.number,
  tapAin: PropTypes.number,
  t: PropTypes.func.isRequired,
};

TechnicalProviderScoreTree.defaultProps = {
  tap: null,
  tapIs: null,
  tapIca: null,
  tapCco: null,
  tapAin: null,
};

export default translate()(TechnicalProviderScoreTree);
