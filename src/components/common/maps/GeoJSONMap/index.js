import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { GeoJSON } from 'react-leaflet';

import DataMap from 'components/common/maps/DataMap';

class GeoJSONMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bounds: null,
      key: null,
    };

    this.onEachFeature = this.onEachFeature.bind(this);
    this.mouseOverFeature = this.mouseOverFeature.bind(this);
    this.mouseOutFeature = this.mouseOutFeature.bind(this);
    this.layerLoaded = this.layerLoaded.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      this.setState({ key: Math.random() });
    }
  }

  onEachFeature(feature, layer) {
    layer.on({
      mouseover: this.mouseOverFeature.bind(layer),
      mouseout: this.mouseOutFeature.bind(layer),
    });
  }

  mouseOverFeature(e) {
    if (!this.props.popupProperty) return;
    const layer = e.target;
    layer.bindTooltip(layer.feature.properties[this.props.popupProperty]).openTooltip();
  }

  mouseOutFeature(e) {
    if (!this.props.popupProperty) return;
    const layer = e.target;
    layer.closeTooltip();
  }

  addData(data) {
    if (this.layer) {
      this.layer.leafletElement.addData(data);
      this.setState({
        bounds: this.layer.leafletElement.getBounds(),
      });
    }
  }

  layerLoaded(event) {
    if (this.props.data) {
      this.setState({
        bounds: event.target.getBounds(),
      });
    }
  }

  render() {
    return (
      <DataMap
        ref={map => (this.map = map)}
        title={this.props.title}
        className={`geojson-map ${this.props.className}`}
        mapCenter={this.props.mapCenter}
        zoomLevel={this.props.zoomLevel}
        sticky={this.props.sticky}
        bounds={this.props.loading ? null : this.state.bounds}
        loading={this.props.loading}
      >
        <GeoJSON
          ref={layer => (this.layer = layer)}
          key={this.state.key}
          style={this.props.mapStyle}
          data={this.props.data}
          onEachFeature={this.onEachFeature}
          pointToLayer={this.props.pointToLayer}
          onAdd={this.layerLoaded}
        />
      </DataMap>
    );
  }
}

GeoJSONMap.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  mapCenter: PropTypes.array.isRequired,
  zoomLevel: PropTypes.number.isRequired,
  data: PropTypes.object,
  popupProperty: PropTypes.string,
  mapStyle: PropTypes.func,
  pointToLayer: PropTypes.func,
  sticky: PropTypes.bool,
  loading: PropTypes.bool,
};

GeoJSONMap.defaultProps = {
  title: null,
  className: null,
  popupProperty: null,
  data: null,
  mapStyle: null,
  pointToLayer: null,
  sticky: false,
  loading: false,
};

export default GeoJSONMap;
