import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Card, CardText, CardBody, CardTitle } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Loading from 'components/common/Loading';
import { formatValue } from 'lib/Utils';

import './style.css';

class ValueBox extends Component {
  render() {
    let value;
    let content;

    if (this.props.value !== null) {
      if (!this.props.format) {
        value = this.props.value;
      } else {
        value = formatValue(this.props.value);
      }
      content = <CardTitle className="value-box-value">{value}</CardTitle>;
    } else {
      content = this.props.children;
    }

    const card = (
      <Card className={`value-box mb-4 ${this.props.className}`}>
        <CardBody>
          {this.props.icon &&
            <FontAwesomeIcon className="value-box-icon" icon={this.props.icon} />
          }
          {content || <Loading />}
          <CardText className="value-box-title">{this.props.title}</CardText>
        </CardBody>
      </Card>
    );

    if (this.props.href) {
      return <Link to={this.props.href} className="value-box-nav">{card}</Link>;
    }

    return card;
  }
}

ValueBox.propTypes = {
  icon: PropTypes.object,
  title: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  children: PropTypes.arrayOf(PropTypes.element),
  format: PropTypes.bool,
  href: PropTypes.string,
  className: PropTypes.string,
};

ValueBox.defaultProps = {
  icon: null,
  value: null,
  children: null,
  format: true,
  href: null,
  className: null,
};

export default ValueBox;
