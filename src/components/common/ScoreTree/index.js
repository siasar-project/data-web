import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './style.css';

class ScoreTree extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        xmlSpace="preserve"
        viewBox="0 0 510 360"
        className="score-tree mb-4"
      >
        {this.props.children}
      </svg>
    );
  }
}

ScoreTree.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default ScoreTree;
