import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import { translate } from 'react-i18next';

import Api from 'lib/Api';

class CountryList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      countries: [],
      isOpen: false,
    };

    this.toggle = this.toggle.bind(this);
    this.update = this.update.bind(this);
  }

  async componentDidMount() {
    const countries = (await Api.get(this.props.endpoint, {
      attributes: ['country', 'adm0'],
      group: ['country', 'adm0'],
      order: ['adm0'],
    })).map(country => ({ code: country.country, name: country.adm0 }));

    const selectedCountry = this.props.country ?
      countries.filter(country => country.code === this.props.country.code)[0] : null;

    this.setState({ countries });

    this.props.onChange(selectedCountry, countries);
  }

  update(event) {
    this.props.onChange(
      event.target.dataset.index ? this.state.countries[event.target.dataset.index] : null,
      this.state.countries,
    );
  }

  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const { t } = this.props;
    return (
      <ButtonDropdown
        size="sm"
        isOpen={this.state.isOpen}
        toggle={this.toggle}
        className={`d-print-none ${this.props.className}`}
      >
        <DropdownToggle caret color="white">{t('country')}</DropdownToggle>
        <DropdownMenu>
          {this.props.all &&
            <DropdownItem data-index="" onClick={this.update}>{t('all')}</DropdownItem>
          }
          {this.state.countries.map((country, index) => (
            <DropdownItem
              key={country.code}
              data-index={index}
              onClick={this.update}
            >{country.name}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

CountryList.propTypes = {
  all: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  endpoint: PropTypes.string,
  country: PropTypes.object,
  className: PropTypes.string,
  t: PropTypes.func.isRequired,
};

CountryList.defaultProps = {
  all: false,
  endpoint: 'communities',
  country: null,
  className: null,
};

export default translate()(CountryList);
