import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { Container, Row } from 'reactstrap';

import ErrorBoundary from './components/common/ErrorBoundary';
import Aside from './components/common/Aside';
import Dashboard from './components/dashboard/Dashboard';
import Communities from './components/community/Communities';
import CommunityList from './components/community/CommunityList';
import Community from './components/community/Community';
import Systems from './components/systems/Systems';
import SystemList from './components/systems/SystemList';
import System from './components/systems/System';
import ServiceProviders from './components/service-providers/ServiceProviders';
import ServiceProviderList from './components/service-providers/ServiceProviderList';
import ServiceProvider from './components/service-providers/ServiceProvider';
import TechnicalProviderList from './components/technical-providers/TechnicalProviderList';
import TechnicalProvider from './components/technical-providers/TechnicalProvider';
import TechnicalProviders from './components/technical-providers/TechnicalProviders';
import Sdgs from './components/sdgs/Sdg';

import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Container fluid className="h-100">
          <Row className="h-100">
            <Aside className="col-12 col-md-4 col-lg-3 col-xl-2 d-print-none" />
            <main className="col bg-faded pt-3 pb-5 h-100">
              <ErrorBoundary>
                <Switch>
                  <Route path="/communities/list" component={CommunityList} />
                  <Route path="/communities/:id(\d+)" component={Community} />
                  <Route path="/communities" component={Communities} />
                  <Route path="/systems/list" component={SystemList} />
                  <Route path="/systems/:id(\d+)" component={System} />
                  <Route path="/systems" component={Systems} />
                  <Route path="/service-providers/list" component={ServiceProviderList} />
                  <Route path="/service-providers/:id(\d+)" component={ServiceProvider} />
                  <Route path="/service-providers" component={ServiceProviders} />
                  <Route path="/technical-providers/list" component={TechnicalProviderList} />
                  <Route path="/technical-providers/:id(\d+)" component={TechnicalProvider} />
                  <Route path="/technical-providers" component={TechnicalProviders} />
                  <Route path="/sdgs" component={Sdgs} />
                  <Route path="/" component={Dashboard} />
                </Switch>
              </ErrorBoundary>
            </main>
          </Row>
        </Container>
      </BrowserRouter>
    );
  }
}

export default App;
